/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-settings-main.h"

#include "mildenhall-settings-view.h"

typedef struct _MildenhallSettingsMain MildenhallSettingsMain;

struct _MildenhallSettingsMain
{
  GObject parent;
  MildenhallSettingsView *main_view;             /* owned */
  MildenhallSettingsController *main_controller; /* owned */
  gchar *settings_app_name;                      /* owned */
  gchar *launch_settings;                        /* owned */
  gchar *setting_view_name;                      /* owned */
};

G_DEFINE_TYPE (MildenhallSettingsMain, mildenhall_settings_main, G_TYPE_OBJECT)

enum
{
  MILDENHALL_SETTINGS_MAIN_APP_NAME = 1,
  MILDENHALL_SETTINGS_MAIN_LAUNCH_SETTINGS,
  MILDENHALL_SETTINGS_MAIN_VIEW_NAME,
  MILDENHALL_SETTINGS_MAIN_ENUM_LAST
};

static void
mildenhall_settings_main_dispose (GObject *object)
{
  MildenhallSettingsMain *self = MILDENHALL_SETTINGS_MAIN (object);

  g_clear_object (&self->main_controller);
  g_clear_object (&self->main_view);
  G_OBJECT_CLASS (mildenhall_settings_main_parent_class)
      ->dispose (object);
}

static void
mildenhall_settings_main_finalize (GObject *object)
{
  MildenhallSettingsMain *self = MILDENHALL_SETTINGS_MAIN (object);

  g_clear_pointer (&self->settings_app_name, g_free);
  g_clear_pointer (&self->launch_settings, g_free);
  g_clear_pointer (&self->setting_view_name, g_free);
  G_OBJECT_CLASS (mildenhall_settings_main_parent_class)
      ->finalize (object);
}

static void
mildenhall_settings_main_constrcuted (GObject *object)
{
  ClutterActor *stage = NULL;
  MildenhallSettingsMain *self = MILDENHALL_SETTINGS_MAIN (object);

  self->main_controller = mildenhall_settings_controller_new (G_OBJECT (self->main_view),
                                                              self->settings_app_name,
                                                              self->launch_settings,
                                                              self->setting_view_name);
  stage = create_mildenhall_window (self->settings_app_name);

  clutter_actor_add_child (CLUTTER_ACTOR (stage),
                           CLUTTER_ACTOR (self->main_view->main_window));
  clutter_actor_show (stage);
}

static void
mildenhall_settings_main_get_property (GObject *object,
                                       guint property_id,
                                       GValue *value,
                                       GParamSpec *pspec)
{
  MildenhallSettingsMain *self = MILDENHALL_SETTINGS_MAIN (object);

  switch (property_id)
    {
    case MILDENHALL_SETTINGS_MAIN_APP_NAME:
      g_value_set_string (value, self->settings_app_name);
      break;

    case MILDENHALL_SETTINGS_MAIN_LAUNCH_SETTINGS:
      g_value_set_string (value, self->launch_settings);
      break;

    case MILDENHALL_SETTINGS_MAIN_VIEW_NAME:
      g_value_set_string (value, self->setting_view_name);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mildenhall_settings_main_set_property (GObject *object,
                                       guint property_id,
                                       const GValue *value,
                                       GParamSpec *pspec)
{
  MildenhallSettingsMain *self = MILDENHALL_SETTINGS_MAIN (object);

  switch (property_id)
    {
    case MILDENHALL_SETTINGS_MAIN_APP_NAME:
      self->settings_app_name = g_value_dup_string (value);
      break;

    case MILDENHALL_SETTINGS_MAIN_LAUNCH_SETTINGS:
      self->launch_settings = g_value_dup_string (value);
      break;

    case MILDENHALL_SETTINGS_MAIN_VIEW_NAME:
      self->setting_view_name = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mildenhall_settings_main_class_init (MildenhallSettingsMainClass *klass)
{
  GParamSpec *pspec = NULL;
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = mildenhall_settings_main_get_property;
  object_class->set_property = mildenhall_settings_main_set_property;
  object_class->dispose = mildenhall_settings_main_dispose;
  object_class->finalize = mildenhall_settings_main_finalize;
  object_class->constructed = mildenhall_settings_main_constrcuted;

  pspec = g_param_spec_string ("app-name", "app-name", "Settings app name", NULL,
                               (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property (object_class, MILDENHALL_SETTINGS_MAIN_APP_NAME, pspec);

  pspec = g_param_spec_string ("launch-settings", "launch-settings", "launch settings name", NULL,
                               (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property (object_class, MILDENHALL_SETTINGS_MAIN_LAUNCH_SETTINGS, pspec);

  pspec = g_param_spec_string ("view-name", "view-name", "Settings view name", NULL,
                               (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property (object_class, MILDENHALL_SETTINGS_MAIN_VIEW_NAME, pspec);
}

static void
mildenhall_settings_main_init (MildenhallSettingsMain *self)
{
  self->main_view = mildenhall_settings_view_new ();
}

MildenhallSettingsMain *
mildenhall_settings_main_new (const gchar *app_name,
                              const gchar *launch_settings,
                              const gchar *view_name)
{
  return g_object_new (MILDENHALL_SETTINGS_TYPE_MAIN,
                       "app-name", app_name,
                       "launch-settings", launch_settings,
                       "view-name", view_name, NULL);
}

int
main (gint argc,
      gchar **argv)
{
  guint index_loop = 0;
  gchar *app_name = NULL;
  gchar *launch_settings = NULL;
  gchar *view_name = NULL;

  initialize_ui_toolkit (argc, argv);

  /*Here 'command line option parser' should be used to parse command line arguments
   *to main function which will support error handling feature.
   *By using GOptionsEntry we can specify the arguments
   *to be passed to main function. Currently, canterbury launches
   *mildenhall-settings binary with arguments like 'app-name', 'launch-setting', 
   *'setting-view-name' respectively. But command line option parser checks for 
   *arguments prefixed with '--', like '--app-name', '--launch-setting', 
   *'--setting-view-name'. Hence, now the parsing will fail and command line 
   * option parser can't used. But changes are planned for future*/
  if (NULL != argv)
    {
      while (argv[index_loop])
        {
          if (g_strcmp0 (argv[index_loop], "app-name") == 0)
            {
              app_name = g_strdup (argv[index_loop + 1]);
            }
          if (g_strcmp0 (argv[index_loop], "launch-setting") == 0)
            {
              launch_settings = g_strdup (argv[index_loop + 1]);
            }
          if (g_strcmp0 (argv[index_loop], "setting-view-name") == 0)
            {
              view_name = g_strdup (argv[index_loop + 1]);
            }
          index_loop++;
        }
    }

  mildenhall_settings_main_new (app_name, launch_settings, view_name);

  clutter_main ();
  return TRUE;
}
