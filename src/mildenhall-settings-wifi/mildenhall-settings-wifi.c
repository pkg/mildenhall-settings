/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

#include "mildenhall-settings-wifi.h"
#include "mildenhall-settings-wifi-backend.h"
#include "mildenhall-settings-wifi-controller.h"
#include "mildenhall-settings-wifi-view.h"
#include "mildenhall-settings.h"
#include <clutter/clutter.h>
#include <glib-object.h>

typedef struct _MildenhallSettingsWifi
{
  GObject parent;

  MildenhallSettingsWifiView *wifi_view;             /* owned */
  MildenhallSettingsWifiController *wifi_controller; /* owned */
  MildenhallSettingsWifiBackend *wifi_backend;       /* owned */
} MildenhallSettingsWifi;

static void
mildenhall_settings_system_iface_init (MildenhallSettingsSystemInterface *iface);

G_DEFINE_TYPE_WITH_CODE (
    MildenhallSettingsWifi,
    mildenhall_settings_wifi,
    G_TYPE_OBJECT,
    G_IMPLEMENT_INTERFACE (MILDENHALL_SETTINGS_TYPE_SYSTEM,
                           mildenhall_settings_system_iface_init))

static ClutterActor *
mildenhall_settings_wifi_get_with_default_view (MildenhallSettingsSystem *setting);
static ClutterActor *
mildenhall_settings_wifi_get_by_view_name (MildenhallSettingsSystem *setting,
                                           const gchar *view_name);
static void
mildenhall_settings_wifi_show_view (MildenhallSettingsSystem *setting,
                                    const gchar *view_name);
static gboolean
mildenhall_settings_wifi_handle_back_press (MildenhallSettingsSystem *setting);

static void
mildenhall_settings_system_iface_init (MildenhallSettingsSystemInterface *iface)
{
  DEBUG ("entered");
  iface->get_with_default_view = mildenhall_settings_wifi_get_with_default_view;
  iface->get_by_view_name = mildenhall_settings_wifi_get_by_view_name;
  iface->show_view = mildenhall_settings_wifi_show_view;
  iface->handle_back_press = mildenhall_settings_wifi_handle_back_press;
}

static void
mildenhall_settings_wifi_dispose (GObject *object)
{
  MildenhallSettingsWifi *mildenhall_settings_wifi = MILDENHALL_SETTINGS_WIFI (object);

  DEBUG ("entered");
  g_clear_object (&mildenhall_settings_wifi->wifi_controller);
  g_clear_object (&mildenhall_settings_wifi->wifi_backend);
  g_clear_object (&mildenhall_settings_wifi->wifi_view);
  G_OBJECT_CLASS (mildenhall_settings_wifi_parent_class)
      ->dispose (object);
}

static void
mildenhall_settings_wifi_class_init (MildenhallSettingsWifiClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->dispose = mildenhall_settings_wifi_dispose;
}

static void
mildenhall_settings_wifi_init (MildenhallSettingsWifi *self)
{
  self->wifi_view = mildenhall_settings_wifi_view_new ();
  self->wifi_backend = mildenhall_settings_wifi_backend_new ();
  self->wifi_controller = mildenhall_settings_wifi_controller_new (self->wifi_view,
                                                                   self->wifi_backend);
}

static ClutterActor *
mildenhall_settings_wifi_get_with_default_view (MildenhallSettingsSystem *setting)
{
  ClutterActor *view_container;
  MildenhallSettingsWifi *wifi_settings = MILDENHALL_SETTINGS_WIFI (setting);

  g_return_val_if_fail (MILDENHALL_SETTINGS_IS_WIFI (setting), NULL);

  view_container = mildenhall_settings_wifi_get_view_container (wifi_settings->wifi_view);
  mildenhall_settings_wifi_show_view_with_name (wifi_settings->wifi_view,
                                                MILDENHALL_SETTINGS_WIFI_DEFAULT_VIEW);
  return view_container;
}

ClutterActor *
mildenhall_settings_wifi_get_by_view_name (MildenhallSettingsSystem *setting,
                                           const gchar *view_name)
{
  ClutterActor *view_container;
  MildenhallSettingsWifi *wifi_settings = MILDENHALL_SETTINGS_WIFI (setting);

  g_return_val_if_fail (MILDENHALL_SETTINGS_IS_WIFI (setting), NULL);

  view_container = mildenhall_settings_wifi_get_view_container (wifi_settings->wifi_view);
  mildenhall_settings_wifi_show_view_with_name (wifi_settings->wifi_view,
                                                view_name);
  return view_container;
}

void
mildenhall_settings_wifi_show_view (MildenhallSettingsSystem *setting,
                                    const gchar *view_name)
{
  MildenhallSettingsWifi *wifi_settings = MILDENHALL_SETTINGS_WIFI (setting);

  g_return_if_fail (MILDENHALL_SETTINGS_IS_WIFI (setting));

  mildenhall_settings_wifi_show_view_with_name (wifi_settings->wifi_view,
                                                view_name);
}

static gboolean
mildenhall_settings_wifi_handle_back_press (MildenhallSettingsSystem *setting)
{
  g_return_val_if_fail (MILDENHALL_SETTINGS_IS_WIFI (setting), FALSE);
  return FALSE;
}
