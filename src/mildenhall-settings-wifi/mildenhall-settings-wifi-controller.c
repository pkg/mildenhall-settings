/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

#include "mildenhall-settings-wifi-controller.h"
#include "mildenhall-settings-wifi-internal.h"
#include "mildenhall-settings.h"
#include <barkway/barkway.h>
#include <canterbury.h>
#include <canterbury/canterbury.h>
#include <clutter/x11/clutter-x11.h>

enum MildenhallSettingsWifiControllerProperties
{
  MILDENHALL_SETTINGS_WIFI_CONTROLLER_ENUM_FIRST,
  MILDENHALL_SETTINGS_WIFI_CONTROLLER_VIEW_OBJECT,
  MILDENHALL_SETTINGS_WIFI_CONTROLLER_BACKEND_OBJECT,
  MILDENHALL_SETTINGS_WIFI_CONTROLLER_ENUM_LAST
};

typedef enum {
  CONNMAN_STATUS_UPDATING,
  CONNMAN_STATUS_UPDATED,
} Connman_wifi_status;

typedef struct _MildenhallSettingsWifiController
{
  GObject parent;

  MildenhallSettingsWifiView *wifi_settings_view;       /* owned */
  MildenhallSettingsWifiBackend *wifi_settings_backend; /* owned */

  BarkwayService *popup_service_proxy; /* owned */
  gulong barkway_handler_id;           /* owned */
  gulong popup_signal_confirm_res;     /* owned */
  gulong popup_signal_status;          /* owned */
  GCancellable *cancellable;           /* owned */

  CanterburyMutterPlugin *mutter_proxy; /* owned */
  gulong mutter_handler_id;             /* owned */

  gulong context_drawer_sig; /* owned */
  const gchar *bundle_id;    /* unowned */

  Connman_wifi_status connman_wifi_status;
} MildenhallSettingsWifiController;

G_DEFINE_TYPE (MildenhallSettingsWifiController,
               mildenhall_settings_wifi_controller,
               G_TYPE_OBJECT)

static void
wifi_busy_animation_started_cb (GObject *source,
                                GAsyncResult *res,
                                gpointer user_data)
{
  GError *error = NULL;
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (user_data);

  canterbury_mutter_plugin_call_start_idle_animation_finish (wifi_controller->mutter_proxy,
                                                             res, &error);
  if (error)
    {
      WARNING ("%s", error->message);
      g_clear_error (&error);
      return;
    }
}

static void
update_busy_animation (MildenhallSettingsWifiController *wifi_controller)
{
  if (wifi_controller->connman_wifi_status == CONNMAN_STATUS_UPDATED)
    return;
  else if (wifi_controller->mutter_proxy)
    {
      canterbury_mutter_plugin_call_start_idle_animation (
          wifi_controller->mutter_proxy, wifi_controller->cancellable,
          wifi_busy_animation_started_cb, wifi_controller);
    }
}

static void
mutter_busy_animation_proxy_clb (GObject *source,
                                 GAsyncResult *res,
                                 gpointer user_data)
{
  GError *error = NULL;
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (user_data);

  DEBUG ("entered");
  wifi_controller->mutter_proxy = canterbury_mutter_plugin_proxy_new_finish (res, &error);
  if (error)
    {
      WARNING ("%s", error->message);
      g_clear_error (&error);
      return;
    }
  else
    {
      update_busy_animation (wifi_controller);
    }
}

static void
mutter_busy_animation_name_appeared (GDBusConnection *connection,
                                     const gchar *name,
                                     const gchar *name_owner,
                                     gpointer user_data)
{
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (user_data);

  canterbury_mutter_plugin_proxy_new (
      connection, G_DBUS_PROXY_FLAGS_NONE,
      "org.apertis.Canterbury.Mutter.Plugin",
      "/org/apertis/Canterbury/Mutter/Plugin",
      wifi_controller->cancellable,
      mutter_busy_animation_proxy_clb, user_data);
}

static void
mutter_busy_animation_name_vanished (GDBusConnection *connection,
                                     const gchar *name,
                                     gpointer user_data)
{
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (user_data);
  g_clear_object (&wifi_controller->mutter_proxy);
}

static gboolean
barkway_popup_confirmation_result_clb (BarkwayService *barkway_service,
                                       const gchar *arg_app_name,
                                       const gchar *arg_title,
                                       const gchar *arg_confirmation_result,
                                       gint confirmation_value,
                                       MildenhallSettingsWifiController *wifi_controller)
{
  DEBUG ("Popup Confirmation handled");
  return TRUE;
}

static gboolean
barkway_popup_status_clb (BarkwayService *barkway_service,
                          const gchar *arg_app_name,
                          const gchar *arg_title,
                          const gchar *arg_popup_status,
                          MildenhallSettingsWifiController *wifi_controller)
{
  DEBUG ("Popup Hidden calling");
  return TRUE;
}

static void
barkway_popup_proxy_clb (GObject *source,
                         GAsyncResult *res,
                         gpointer user_data)
{
  GError *error = NULL;
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (user_data);

  DEBUG ("entered");
  wifi_controller->popup_service_proxy = barkway_service_proxy_new_finish (res, &error);
  if (error)
    {
      WARNING ("%s", error->message);
      g_clear_error (&error);
      return;
    }
  else
    {
      wifi_controller->popup_signal_confirm_res = g_signal_connect (
          wifi_controller->popup_service_proxy, "confirmation-result",
          G_CALLBACK (barkway_popup_confirmation_result_clb), wifi_controller);
      wifi_controller->popup_signal_status = g_signal_connect (
          wifi_controller->popup_service_proxy, "popup-status",
          G_CALLBACK (barkway_popup_status_clb), wifi_controller);
    }
}

static void
barkway_popup_name_appeared (GDBusConnection *connection,
                             const gchar *name,
                             const gchar *name_owner,
                             gpointer user_data)
{
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (user_data);

  /* Creates a new proxy for a remote interface exported by a connection on a message bus */
  barkway_service_proxy_new (connection, G_DBUS_PROXY_FLAGS_NONE,
                             "org.apertis.Barkway",
                             "/org/apertis/Barkway/Service",
                             wifi_controller->cancellable,
                             barkway_popup_proxy_clb, user_data);
}

static void
barkway_popup_clear (MildenhallSettingsWifiController *wifi_controller)
{
  g_signal_handler_disconnect (wifi_controller->popup_service_proxy,
                               wifi_controller->popup_signal_confirm_res);
  g_signal_handler_disconnect (wifi_controller->popup_service_proxy,
                               wifi_controller->popup_signal_status);
  g_clear_object (&wifi_controller->popup_service_proxy);
  wifi_controller->popup_signal_confirm_res = 0;
  wifi_controller->popup_signal_status = 0;
}

static void
barkway_popup_name_vanished (GDBusConnection *connection,
                             const gchar *name,
                             gpointer user_data)
{
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (user_data);
  barkway_popup_clear (wifi_controller);
}

static void
mildenhall_settings_wifi_controller_dispose (GObject *object)
{
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (object);

  DEBUG ("entered");
  g_cancellable_cancel (wifi_controller->cancellable);
  g_clear_object (&wifi_controller->cancellable);

  barkway_popup_clear (wifi_controller);
  if (wifi_controller->barkway_handler_id)
    {
      g_bus_unwatch_name (wifi_controller->barkway_handler_id);
      wifi_controller->barkway_handler_id = 0;
    }
  if (wifi_controller->mutter_handler_id)
    {
      g_bus_unwatch_name (wifi_controller->mutter_handler_id);
      wifi_controller->mutter_handler_id = 0;
    }
  g_clear_object (&wifi_controller->mutter_proxy);
  g_clear_object (&wifi_controller->wifi_settings_backend);

  g_signal_handler_disconnect (wifi_controller->wifi_settings_view,
                               wifi_controller->context_drawer_sig);
  wifi_controller->context_drawer_sig = 0;
  g_clear_object (&wifi_controller->wifi_settings_view);

  G_OBJECT_CLASS (mildenhall_settings_wifi_controller_parent_class)
      ->dispose (object);
}

static void
mildenhall_settings_wifi_controller_on_show_popup_clb (GObject *source,
                                                       GAsyncResult *res,
                                                       gpointer user_data)
{
  GError *error = NULL;
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (user_data);
  barkway_service_call_show_popup_finish (wifi_controller->popup_service_proxy, res, &error);
  if (error)
    {
      WARNING ("%s", error->message);
      g_clear_error (&error);
      return;
    }
}

static void
wifi_busy_animation_stopped_cb (GObject *source,
                                GAsyncResult *res,
                                gpointer user_data)
{
  GError *error = NULL;
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (user_data);
  canterbury_mutter_plugin_call_stop_idle_animation_finish (wifi_controller->mutter_proxy,
                                                            res, &error);
  if (error)
    {
      WARNING ("%s", error->message);
      g_clear_error (&error);
      return;
    }
}

static void
mildenhall_settings_wifi_controller_show_popup (gint popup_type,
                                                const gchar *text,
                                                gpointer user_data)
{
  GVariantBuilder gvb1, gvb2, gvb3;
  const gchar *arg_title = "POPUP";
  const gchar *priority = "HIGH";
  const gchar *sound = "NULL";
  gint voice_control_time = 0.0;
  gdouble time_out = 4;
  GVariant *popup_action;
  GVariant *image_data;
  GVariant *display_text;

  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (user_data);

  g_variant_builder_init (&gvb1, G_VARIANT_TYPE ("a{ss}"));
  g_variant_builder_add (&gvb1, "{ss}", "Text-Active", text);
  display_text = g_variant_builder_end (&gvb1);

  g_variant_builder_init (&gvb2, G_VARIANT_TYPE ("a{ss}"));
  if (BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_ONE_WITH_TEXT == popup_type)
    {
      g_variant_builder_add (&gvb2, "{ss}", "Close", "CLOSE");
    }
  popup_action = g_variant_builder_end (&gvb2);

  g_variant_builder_init (&gvb3, G_VARIANT_TYPE ("a{ss}"));
  g_variant_builder_add (&gvb3, "{ss}", "AppIcon", WIFI_OFF_AC);
  g_variant_builder_add (&gvb3, "{ss}", "MsgIcon", WIFI_OFF_AC);
  image_data = g_variant_builder_end (&gvb3);

  barkway_service_call_show_popup (
      wifi_controller->popup_service_proxy, wifi_controller->bundle_id, popup_type,
      arg_title, display_text, priority, popup_action, image_data, time_out,
      sound, voice_control_time, wifi_controller->cancellable,
      mildenhall_settings_wifi_controller_on_show_popup_clb, wifi_controller);
}

static void
mildenahll_settings_wifi_controller_show_error_cb (MildenhallSettingsWifiBackend *wifi_backend,
                                                   const gchar *popup_msg,
                                                   gpointer user_data)
{
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (user_data);

  canterbury_mutter_plugin_call_stop_idle_animation (
      wifi_controller->mutter_proxy, wifi_controller->cancellable,
      wifi_busy_animation_stopped_cb, user_data);
  if (popup_msg != NULL)
    {
      mildenhall_settings_wifi_controller_show_popup (
          BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_ONE_WITH_TEXT, popup_msg,
          wifi_controller);
    }
}

static void
mildenhall_settings_wifi_controller_context_drawer_pressed_cb (GObject *object,
                                                               gchar *button_name,
                                                               gpointer user_data)
{

  gboolean wifi_status;
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (user_data);

  DEBUG ("Entered");
  if (!g_strcmp0 (button_name, WIFI_POWERED_STATUS))
    {
      g_object_get (wifi_controller->wifi_settings_backend, "wifi-status",
                    &wifi_status, NULL);
      wifi_controller->connman_wifi_status = CONNMAN_STATUS_UPDATING;
      update_busy_animation (wifi_controller);
      mildenhall_settings_wifi_backend_set_wifi_status (
          wifi_controller->wifi_settings_backend, !wifi_status);
    }
}

static void
mildenhall_settings_wifi_controller_status_changed (GObject *gobject,
                                                    GParamSpec *pspec,
                                                    gpointer user_data)
{
  gboolean wifi_status;
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (user_data);
  wifi_controller->connman_wifi_status = CONNMAN_STATUS_UPDATED;
  canterbury_mutter_plugin_call_stop_idle_animation (wifi_controller->mutter_proxy,
                                                     wifi_controller->cancellable,
                                                     wifi_busy_animation_stopped_cb,
                                                     user_data);
  g_object_get (wifi_controller->wifi_settings_backend, "wifi-status", &wifi_status, NULL);
  g_object_set (wifi_controller->wifi_settings_view, "state", wifi_status, NULL);
  mildenhall_settings_wifi_controller_show_popup (BARKWAY_POPUP_TYPE_ENUM_CONFIRM_OPTION_ONE_WITH_TEXT,
                                                  wifi_status ? WIFI_ON : WIFI_OFF, wifi_controller);
}

static void
mildenhall_settings_wifi_controller_get_property (GObject *object,
                                                  guint property_id,
                                                  GValue *value,
                                                  GParamSpec *pspec)
{
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (object);

  switch (property_id)
    {
    case MILDENHALL_SETTINGS_WIFI_CONTROLLER_VIEW_OBJECT:
      g_value_set_object (value, wifi_controller->wifi_settings_view);
      break;

    case MILDENHALL_SETTINGS_WIFI_CONTROLLER_BACKEND_OBJECT:
      g_value_set_object (value, wifi_controller->wifi_settings_backend);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mildenhall_settings_wifi_controller_set_property (GObject *object,
                                                  guint property_id,
                                                  const GValue *value,
                                                  GParamSpec *pspec)
{
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (object);

  switch (property_id)
    {
    case MILDENHALL_SETTINGS_WIFI_CONTROLLER_VIEW_OBJECT:
      wifi_controller->wifi_settings_view = g_value_dup_object (value);
      break;

    case MILDENHALL_SETTINGS_WIFI_CONTROLLER_BACKEND_OBJECT:
      wifi_controller->wifi_settings_backend = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mildenhall_settings_wifi_controller_constructed (GObject *object)
{
  MildenhallSettingsWifiController *wifi_controller = MILDENHALL_SETTINGS_WIFI_CONTROLLER (object);

  wifi_controller->context_drawer_sig = g_signal_connect (wifi_controller->wifi_settings_view,
                                                          "wifi-detailed-context-button-released",
                                                          G_CALLBACK (mildenhall_settings_wifi_controller_context_drawer_pressed_cb), wifi_controller);
  g_signal_connect (wifi_controller->wifi_settings_backend,
                    "notify::wifi-status", G_CALLBACK (mildenhall_settings_wifi_controller_status_changed),
                    wifi_controller);
  g_signal_connect (wifi_controller->wifi_settings_backend, "wifi-settings-error-signal",
                    G_CALLBACK (mildenahll_settings_wifi_controller_show_error_cb), wifi_controller);
}

static void
mildenhall_settings_wifi_controller_class_init (MildenhallSettingsWifiControllerClass *klass)
{
  GParamSpec *pspec = NULL;
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = mildenhall_settings_wifi_controller_get_property;
  object_class->set_property = mildenhall_settings_wifi_controller_set_property;
  object_class->dispose = mildenhall_settings_wifi_controller_dispose;
  object_class->constructed = mildenhall_settings_wifi_controller_constructed;

  pspec = g_param_spec_object ("view-object", "view-object",
                               "Settings WiFi View object",
                               MILDENHALL_SETTINGS_TYPE_WIFI_VIEW,
                               (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property (
      object_class, MILDENHALL_SETTINGS_WIFI_CONTROLLER_VIEW_OBJECT, pspec);

  pspec = g_param_spec_object ("backend-object", "backend-object",
                               "Settings WiFi Backend object",
                               MILDENHALL_SETTINGS_TYPE_WIFI_BACKEND,
                               (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property (
      object_class, MILDENHALL_SETTINGS_WIFI_CONTROLLER_BACKEND_OBJECT, pspec);
}

static void
mildenhall_settings_wifi_controller_init (MildenhallSettingsWifiController *wifi_controller)
{
  CbyProcessInfo *proc_inf;
  DEBUG ("entered");
  wifi_controller->cancellable = g_cancellable_new ();
  wifi_controller->connman_wifi_status = CONNMAN_STATUS_UPDATING;
  wifi_controller->barkway_handler_id = g_bus_watch_name (G_BUS_TYPE_SESSION,
                                                          "org.apertis.Barkway",
                                                          G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                                                          barkway_popup_name_appeared,
                                                          barkway_popup_name_vanished,
                                                          wifi_controller, NULL);
  wifi_controller->mutter_handler_id = g_bus_watch_name (G_BUS_TYPE_SESSION,
                                                         "org.apertis.Canterbury",
                                                         G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                                                         mutter_busy_animation_name_appeared,
                                                         mutter_busy_animation_name_vanished,
                                                         wifi_controller, NULL);

  proc_inf = cby_process_info_get_self ();
  wifi_controller->bundle_id = cby_process_info_get_bundle_id (proc_inf);
}

MildenhallSettingsWifiController *
mildenhall_settings_wifi_controller_new (MildenhallSettingsWifiView *wifi_view,
                                         MildenhallSettingsWifiBackend *wifi_backend)
{
  return g_object_new (MILDENHALL_SETTINGS_TYPE_WIFI_CONTROLLER,
                       "view-object", wifi_view,
                       "backend-object", wifi_backend,
                       NULL);
}
