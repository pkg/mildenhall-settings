/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

#ifndef MILDENHALL_SETTINGS_WIFI_VIEW_H
#define MILDENHALL_SETTINGS_WIFI_VIEW_H

#include <clutter/clutter.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define MILDENHALL_SETTINGS_TYPE_WIFI_VIEW \
  mildenhall_settings_wifi_view_get_type ()
G_DECLARE_FINAL_TYPE (MildenhallSettingsWifiView, mildenhall_settings_wifi_view, MILDENHALL_SETTINGS_WIFI, VIEW, GObject)

#define MILDENHALL_SETTINGS_WIFI_DEFAULT_VIEW "wifi-default-view"

MildenhallSettingsWifiView *
mildenhall_settings_wifi_view_new (void);
ClutterActor *
mildenhall_settings_wifi_get_view_container (MildenhallSettingsWifiView *view);
void
mildenhall_settings_wifi_show_view_with_name (MildenhallSettingsWifiView *view,
                                              const gchar *view_name);

G_END_DECLS

#endif /* _MILDENHALL_SETTINGS_WIFI_VIEW_H */
