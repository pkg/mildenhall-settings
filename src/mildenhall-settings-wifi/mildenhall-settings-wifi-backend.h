/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

#ifndef _MILDENHALL_SETTINGS_WIFI_BACKEND_H
#define _MILDENHALL_SETTINGS_WIFI_BACKEND_H

#include <clutter/clutter.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define MILDENHALL_SETTINGS_TYPE_WIFI_BACKEND \
  mildenhall_settings_wifi_backend_get_type ()
G_DECLARE_FINAL_TYPE (MildenhallSettingsWifiBackend,
                      mildenhall_settings_wifi_backend,
                      MILDENHALL_SETTINGS_WIFI,
                      BACKEND,
                      GObject)

MildenhallSettingsWifiBackend *
mildenhall_settings_wifi_backend_new (void);
void mildenhall_settings_wifi_backend_set_wifi_status (MildenhallSettingsWifiBackend *wifi_backend,
                                                       gboolean toggled_wifi_status);

G_END_DECLS

#endif /* _MILDENHALL_SETTINGS_WIFI_BACKEND_H */
