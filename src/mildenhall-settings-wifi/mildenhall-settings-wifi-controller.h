/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

#ifndef _MILDENHALL_SETTINGS_WIFI_CONTROLLER_H
#define _MILDENHALL_SETTINGS_WIFI_CONTROLLER_H

#include "mildenhall-settings-wifi-backend.h"
#include "mildenhall-settings-wifi-view.h"

G_BEGIN_DECLS

#define MILDENHALL_SETTINGS_TYPE_WIFI_CONTROLLER \
  mildenhall_settings_wifi_controller_get_type ()
G_DECLARE_FINAL_TYPE (MildenhallSettingsWifiController,
                      mildenhall_settings_wifi_controller,
                      MILDENHALL_SETTINGS_WIFI,
                      CONTROLLER,
                      GObject)

MildenhallSettingsWifiController *
mildenhall_settings_wifi_controller_new (MildenhallSettingsWifiView *view,
                                         MildenhallSettingsWifiBackend *backend);

G_END_DECLS

#endif /* _MILDENHALL_SETTINGS_WIFI_CONTROLLER_H */
