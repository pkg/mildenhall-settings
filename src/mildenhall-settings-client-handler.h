/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _MILDENHALL_SETTINGS_CLIENT_HANDLER_H
#define _MILDENHALL_SETTINGS_CLIENT_HANDLER_H

#include <glib-object.h>
#include <canterbury.h>
#include <stdlib.h>
#include <clutter/clutter.h>
#include "mildenhall-settings.h"

G_BEGIN_DECLS

#define MILDENHALL_SETTINGS_TYPE_CLIENT_HANDLER mildenhall_settings_client_handler_get_type()
G_DECLARE_FINAL_TYPE (MildenhallSettingsClientHandler,
                      mildenhall_settings_client_handler, MILDENHALL_SETTINGS,
                      CLIENT_HANDLER, GObject)

typedef struct _MildenhallSettingsAppListData
{
  gchar *icon;
  gchar *label;
  gchar *settings_path;
} MildenhallSettingsAppListData;

typedef struct _MildenhallSettingsClientHandler
{
  GObject parent;
  gchar *app_name; /* owned */
  CanterburyAppManager *app_mgr_proxy; /* owned */
  CanterburyHardKeys *hardkeys_mgr_proxy; /* owned */
  guint watcher_id;
  GSList *list_of_app_data; /* owned */
} MildenhallSettingsClientHandler;

MildenhallSettingsClientHandler *mildenhall_settings_client_handler_new (const gchar *app_name);
G_END_DECLS

#endif /* _MILDENHALL_SETTINGS_CLIENT_HANDLER_H */
