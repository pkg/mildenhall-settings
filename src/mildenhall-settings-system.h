/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _MILDENHALL_SETTINGS_SYSTEM_H
#define _MILDENHALL_SETTINGS_SYSTEM_H

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define MILDENHALL_SETTINGS_TYPE_SYSTEM mildenhall_settings_system_get_type()
G_DECLARE_INTERFACE  (MildenhallSettingsSystem, mildenhall_settings_system, MILDENHALL_SETTINGS, SYSTEM, GObject)

typedef struct _MildenhallSettingsSystem MildenhallSettingsSystem;
typedef struct _MildenhallSettingsSystemInterface MildenhallSettingsSystemInterface;

struct _MildenhallSettingsSystemInterface
{
  GTypeInterface parent;

  ClutterActor *(*get_with_default_view) (MildenhallSettingsSystem *self);
  ClutterActor *(*get_by_view_name) (MildenhallSettingsSystem *self,
				     const gchar *view_name);
  void (*show_view) (MildenhallSettingsSystem *self,
		     const gchar *view_name);
  gboolean (*handle_back_press) (MildenhallSettingsSystem *self);
};

ClutterActor *mildenhall_settings_system_get_with_default_view (MildenhallSettingsSystem *self);
ClutterActor *mildenhall_settings_system_get_by_view_name (MildenhallSettingsSystem *self,
                                                           const gchar *view_name);
void mildenhall_settings_system_show_view (MildenhallSettingsSystem *self,
                                           const gchar *view_name);
gboolean mildenhall_settings_system_handle_back_press (MildenhallSettingsSystem *self);

G_END_DECLS

#endif /* _MILDENHALL_SETTINGS_SYSTEM_H */
